﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random=UnityEngine.Random;
public class text_script : MonoBehaviour {
	private Text textcomp;
	public list_words words;
	public bool side;
	public blue_controller blue;
	public blue_controller green;
	// Use this for initialization
	void Start () {
		trigger ();

	}
	// Update is called once per frame
	public void OnClick(){
		if (side == false) {
			blue.change = 1;
			green.change = -1;
			trigger ();

		} else {
			blue.change = -1;
			green.change = 1;
			trigger ();

		}

	}



	void trigger () {
			textcomp= GetComponent<Text>();
			float choice = Random.Range (1f, 2f);
			if (choice < 1.5f) {
				side = false;
				textcomp.text = words.blue_likes [Random.Range (0, words.blue_likes.Length)];
			} else {
				side =true;
				textcomp.text = words.green_likes [Random.Range (0, words.green_likes.Length)];
			}
		}
	}
