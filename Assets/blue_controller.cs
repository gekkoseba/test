﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blue_controller : MonoBehaviour {

	public int emotion = 0;
	public int change = 0;
	public float timer=0;
	public Animator anim;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		anim.SetInteger ("change", change);
		if(change!=0){
			timer += Time.deltaTime;
			if (timer >= 3) {
				anim.SetFloat ("Time", timer);
				timer = 0;
				change = 0;
			}
		}
	}
}
	